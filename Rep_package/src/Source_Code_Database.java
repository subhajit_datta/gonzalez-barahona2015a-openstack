
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class Source_Code_Database {
	
	public static void Main(){
		
		System.out.println("Source_Code_Database");	
		
		String SourceCodeUUID[]=GenerateSourceCodeUUID();
		
		System.out.println("UUID length "+SourceCodeUUID.length);
		
		int[] commitCount=generateUuidCommmitCount(SourceCodeUUID);
		
		System.out.println("Commit Count Length "+commitCount.length);
		
		int[] repositoryCount=generateUuidRepositoryCount(SourceCodeUUID);
		
		System.out.println("repository Count Length "+repositoryCount.length);
		
		String[] commitIntervalHours = generateUuidCommitInterval(SourceCodeUUID);
		
		System.out.println("Commit Interval Hours "+commitIntervalHours.length);
		
		for(int x=0;x<commitCount.length;x++){
		//	System.out.println("row : "+x+"\t\tUUID : "+SourceCodeUUID[x]+ "\t\tCommit Count : "+commitCount[x]+"\t\tCommitIntervalHours : "+commitIntervalHours[x]+"\t\t\tRepositoryCount : "+repositoryCount[x]);
		}		
	
	}


	public static String[] GenerateSourceCodeUUID(){
		int rowCount=0;
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT *, count(*) as counts FROM source_code.people_uidentities group by source_code.people_uidentities.uuid order by source_code.people_uidentities.uuid;");
			
			while(rs.next()){
				rowCount++;
			//	System.out.println("Row Count : "+rowCount+"\tUUID : "+rs.getString("uuid")+"\t\tCounts : "+rs.getString("counts"));
			}
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		String UUID[] = new String[rowCount];
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT *, count(*) as counts FROM source_code.people_uidentities group by source_code.people_uidentities.uuid order by source_code.people_uidentities.uuid;");
			
			for(int x=0;rs.next();x++){
				UUID[x]=rs.getString("uuid");
			}			
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		return UUID;
	}

	public static int[] generateUuidCommmitCount(String[] SourceCodeUUID){
		
		int rowCount=0;
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT *, COUNT(*) AS counts FROM source_code.scmlog INNER JOIN source_code.people_uidentities ON source_code.scmlog.committer_id = source_code.people_uidentities.people_id group by source_code.people_uidentities.uuid order by source_code.people_uidentities.uuid;");
			
			while(rs.next()){
				rowCount++;
			}
			//System.out.println("ROW COUNT COMMIT : "+rowCount);
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
////////////////////////////////////////////////////////////////////////	//////////////////////////////////////////////////////	//////////////////	//////////////////////////////////////////////////////	//////////////////		
		
		String UUID[]= new String[rowCount];
		int[] commitCount= new int[rowCount];
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT *, COUNT(*) AS counts FROM source_code.scmlog INNER JOIN source_code.people_uidentities ON source_code.scmlog.committer_id = source_code.people_uidentities.people_id group by source_code.people_uidentities.uuid order by source_code.people_uidentities.uuid;");
			
			for(int x=0;rs.next();x++){
				UUID[x]=rs.getString("uuid");
				commitCount[x]= Integer.parseInt(rs.getString("counts"))  ;
				//System.out.println("row : "+x+"\tUUID : "+UUID[x] +"\tCounts : "+commitCount[x]);
			}
			
			/*
			while(rs.next()){
				
				//System.out.println("UUID : "+rs.getString("uuid")+"\t\tCounts : "+rs.getString("counts"));
			}
			*/
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
////////////////////////////////////////////////////////////////////////	//////////////////////////////////////////////////////	//////////////////	//////////////////////////////////////////////////////	//////////////////		
		
		int UUID_TotalCommitCount[]= new int[SourceCodeUUID.length];
		
		for(int x=0;x<SourceCodeUUID.length;x++){
			UUID_TotalCommitCount[x]=0;
		}
		
		for(int x=0;x<SourceCodeUUID.length;x++){
			for(int y=0;y<UUID.length;y++){
				if(SourceCodeUUID[x].equals(UUID[y])){
					UUID_TotalCommitCount[x]=commitCount[y];
				}
			}
		}

		
		/*
		for(int x=0;x<SourceCodeUUID.length;x++){
			System.out.println("ROW : "+x+"\tUUID : "+SourceCodeUUID[x]+"\tTOTAL COUNT : "+UUID_TotalCommitCount[x]);
			
			if(UUID_TotalCommitCount[x]==0){
				System.out.println("ROW : "+x+"\tUUID : "+SourceCodeUUID[x]+"\tTOTAL COUNT : "+UUID_TotalCommitCount[x]);
			}
			
		}
		 */
		
		return UUID_TotalCommitCount;
		
	}

	public static String[] generateUuidCommitInterval(String[] SourceCodeUUID){
		
		
		int rowCount=0;
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT * FROM source_code.scmlog INNER JOIN source_code.people_uidentities ON source_code.scmlog.committer_id = source_code.people_uidentities.people_id group by source_code.people_uidentities.uuid;");		
			
			for(int x=0;rs.next();x++){
				rowCount++;
			}
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
//////////////////////////////////////////////////////	//////////////////	//////////////////////////////////////////////////////	//////////////////	//////////////////////////////////////////////////////	//////////////////		
	
		String[] UUID= new String[rowCount];
		String[] FirstCommitDate= new String[rowCount];//first commit date
		String[] LastCommitDate= new String[rowCount];//last commit date
		long[] hourInterval = new long[rowCount];
		
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT source_code.people_uidentities.uuid , min(source_code.scmlog.date) as date FROM source_code.scmlog INNER JOIN source_code.people_uidentities ON source_code.scmlog.committer_id = source_code.people_uidentities.people_id group by source_code.people_uidentities.uuid;");
						
			for(int x=0;rs.next();x++){
				UUID[x]=rs.getString("uuid");
				FirstCommitDate[x]=rs.getString("date");
			}
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
////////////////////////////////////////////////////////////////////////	//////////////////////////////////////////////////////	//////////////////	//////////////////////////////////////////////////////	//////////////////		
				
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT source_code.people_uidentities.uuid , max(source_code.scmlog.date) as date FROM source_code.scmlog INNER JOIN source_code.people_uidentities ON source_code.scmlog.committer_id = source_code.people_uidentities.people_id group by source_code.people_uidentities.uuid;");
			
			for(int x=0;rs.next();x++){
				LastCommitDate[x]=rs.getString("date");
				}
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
////////////////////////////////////////////////////////////////////////	//////////////////////////////////////////////////////	//////////////////	//////////////////////////////////////////////////////	//////////////////		
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.0"); 
		for(int x=0;x<UUID.length;x++){
			 LocalDateTime firstCommit = LocalDateTime.parse(FirstCommitDate[x], formatter);
			 LocalDateTime lastCommit = LocalDateTime.parse(LastCommitDate[x], formatter );
			 long minutes = Duration.between(firstCommit, lastCommit).toHours();
			 hourInterval[x]=minutes;
		}
		
		/*
		for(int x=0;x<UUID.length;x++){
			System.out.println("Row : "+x+"\t\tUUID : "+UUID[x]+"\t\tFirst Commit DATE : "+FirstCommitDate[x]+"\t\tLast Commit DATE : "+LastCommitDate[x]+"\t\tHour Interval : "+hourInterval[x]);
		}
		*/
		String[] SourceCodeUuidCommitHourInterval = new String[SourceCodeUUID.length];
		
		for(int x=0;x<SourceCodeUUID.length;x++){			
			outerLoop:
			for(int y=0;y<UUID.length;y++){
				if(SourceCodeUUID[x].equals(UUID[y])){
					SourceCodeUuidCommitHourInterval[x]=Long.toString(hourInterval[y]);
					break outerLoop;
				}else{
					//SourceCodeUuidCommitHourInterval[x]="N/A";
					SourceCodeUuidCommitHourInterval[x]="0";

				}
			}
		}
		/*
		for(int x=0;x<SourceCodeUUID.length;x++){
			System.out.println("UUID : "+SourceCodeUUID[x]+"\t\tInterval Hours : "+ SourceCodeUuidHourInterval[x]);
		}
		*/
		return SourceCodeUuidCommitHourInterval;
	}
	
	public static int[] generateUuidRepositoryCount(String[] SourceCodeUUID){
			
		
		int rowCount=0;
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT *, COUNT(DISTINCT (source_code.scmlog.repository_id)) as counts FROM source_code.scmlog INNER JOIN source_code.people_uidentities ON source_code.scmlog.committer_id = source_code.people_uidentities.people_id GROUP BY source_code.people_uidentities.uuid order by source_code.people_uidentities.uuid;");
			
			while(rs.next()){
				rowCount++;
			}
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
////////////////////////////////////////////////////////////////////////	//////////////////////////////////////////////////////	//////////////////	//////////////////////////////////////////////////////	//////////////////			
		
		String[] UUID = new String[rowCount];
		int[] repositoryCount= new int[rowCount];
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT *, COUNT(DISTINCT (source_code.scmlog.repository_id)) as counts FROM source_code.scmlog INNER JOIN source_code.people_uidentities ON source_code.scmlog.committer_id = source_code.people_uidentities.people_id GROUP BY source_code.people_uidentities.uuid order by source_code.people_uidentities.uuid;");
			
			for(int x=0;rs.next();x++){
				//System.out.println("UUID :" +rs.getString("uuid")+"\t\tCount : "+rs.getString("counts"));UUID
				UUID[x]=rs.getString("uuid");
				repositoryCount[x]=Integer.parseInt(rs.getString("counts")) ;
			}
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
////////////////////////////////////////////////////////////////////////	//////////////////////////////////////////////////////	//////////////////	//////////////////////////////////////////////////////	//////////////////		

		int[] totalRepositoryCount=new int[SourceCodeUUID.length];
		
		for(int x=0;x<totalRepositoryCount.length;x++){
			totalRepositoryCount[x]=0;
		}
		
		for(int x=0;x<totalRepositoryCount.length;x++){
			for(int y=0;y<repositoryCount.length;y++){
				if(SourceCodeUUID[x].equals(UUID[y])){
					totalRepositoryCount[x]=repositoryCount[y];
				}
			}
		}
		
		/*
		for(int x=0;x<totalRepositoryCount.length;x++){
			System.out.println("UUID : "+SourceCodeUUID[x]+"\t\tTotal Count : "+totalRepositoryCount[x]);
		}
		*/
		return totalRepositoryCount;
	}
} 	