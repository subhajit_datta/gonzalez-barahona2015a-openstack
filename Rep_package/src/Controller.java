
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class Controller {

	public static void main(String[] args) {
		/*

		String[] UUIDallDB= UUIDinALLdb.GenerateUuidInAllDB();

		String[] ML_UUID= Mailing_List_Database.GenerateMailingListUUID();
		int[] ML_Messages_Sent_Count=Mailing_List_Database.GenerateMessagesSentCount(ML_UUID);
		String[] ML_MessageInterval=Mailing_List_Database.generateMessagesInterval(ML_UUID);
		int[] ML_MailingListUsedCount=Mailing_List_Database.GenerateMailingListUsedCount(ML_UUID);

		int[] UUIDallDB_Messages_Sent_Count=new int[UUIDallDB.length];
		String[] UUIDallDB_MessageInterval=new String[UUIDallDB.length];
		int[] UUIDallDB_MailingListUsedCount=new int[UUIDallDB.length];



		for(int x=0;x<UUIDallDB.length;x++){
			outerLoop:
			for(int y=0;y<ML_UUID.length;y++){
				if(UUIDallDB[x].equals(ML_UUID[y])){
					UUIDallDB_Messages_Sent_Count[x]=ML_Messages_Sent_Count[y];
					UUIDallDB_MessageInterval[x]=ML_MessageInterval[y];
					UUIDallDB_MailingListUsedCount[x]=ML_MailingListUsedCount[y];
					break outerLoop;
				}else{
					UUIDallDB_Messages_Sent_Count[x]=0;
					UUIDallDB_MessageInterval[x]="N/A";
					UUIDallDB_MailingListUsedCount[x]=0;
				}
			}
		}

		for(int x=0;x<UUIDallDB.length;x++){
			System.out.println("ROW : "+x+"\t\tUUID : "+UUIDallDB[x]+"\t\tMessages Sent Count : "+UUIDallDB_Messages_Sent_Count[x]+
					"\t\tMessage Interval(Hours) : "+UUIDallDB_MessageInterval[x]+"\t\tMailing List Used : "+UUIDallDB_MailingListUsedCount[x]);
		}

	try {

			PrintWriter writer = new PrintWriter(
			new FileWriter("C:\\Users\\moses\\Desktop\\TEST4.csv", true));
		
			writer.append("ROW");
			writer.append(",");
			writer.append("UUID");
			writer.append(",");
			writer.append("Messages Sent Count");
			writer.append(",");
			writer.append("Message Interval(Hours)");
			writer.append(",");
			writer.append("Mailing List Used");
			writer.append("\n");
						
			
			for(int x=0;x<UUIDallDB.length;x++){
				writer.append(""+x);
				writer.append(",");
				writer.append(UUIDallDB[x]);
				writer.append(",");
				writer.append(""+UUIDallDB_Messages_Sent_Count[x]);
				writer.append(",");
				writer.append(""+UUIDallDB_MessageInterval[x]);
				writer.append(",");
				writer.append(""+UUIDallDB_MailingListUsedCount[x]);
				writer.append("\n");
			}

		

			writer.close();
			System.out.println("done");
			
		} catch (IOException E) {
		
		}
	*/
////////////////////////////////////////////////		
		

  
  
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	//////////////////////////////////////////////////////	//////////////////		
 		System.out.println("///////////////////////////UUID ALL Datebase///////////////////////////");
		String[] UUIDallDB= UUIDinALLdb.GenerateUuidInAllDB();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	//////////////////////////////////////////////////////	//////////////////		
		
		System.out.println("///////////////////////////Tickets Datebase///////////////////////////");
		String[] TDB_UUID=Tickets_Database.GenerateTicketsUUID();
		int[] TDB_CommentsCount=Tickets_Database.GenerateUuidTotalComments(TDB_UUID);
		int[] TDB_ChangesCount=Tickets_Database.GenerateUuidTotalChanges(TDB_UUID);
		int[] TDB_IssuesSubmittedCount=Tickets_Database.GenerateUuidTotalSubmittedIssues(TDB_UUID);
		int[] TDB_IssuesAssigned=Tickets_Database.GenerateUuidTotalAssignedIssues(TDB_UUID);
		System.out.println("///////////////////////////Tickets Datebase///////////////////////////");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	//////////////////		
		
		System.out.println("///////////////////////////Source Code Datebase///////////////////////////");
		String[] SC_UUID=Source_Code_Database.GenerateSourceCodeUUID();
		int[] SC_CommitCount=Source_Code_Database.generateUuidCommmitCount(SC_UUID);
		String[] SC_CommitInterval=Source_Code_Database.generateUuidCommitInterval(SC_UUID);
		int[] SC_RepositoryCount=Source_Code_Database.generateUuidRepositoryCount(SC_UUID);
		System.out.println("///////////////////////////Source Code Datebase///////////////////////////");
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	//////////////////////////////////////////////////////	//////////////////		
		
		System.out.println("///////////////////////////Mailing List ///////////////////////////");
		String[] ML_UUID= Mailing_List_Database.GenerateMailingListUUID();
		int[] ML_Messages_Sent_Count=Mailing_List_Database.GenerateMessagesSentCount(ML_UUID);
		String[] ML_MessageInterval=Mailing_List_Database.generateMessagesInterval(ML_UUID);
		int[] ML_MailingListUsedCount=Mailing_List_Database.GenerateMailingListUsedCount(ML_UUID);
		System.out.println("///////////////////////////Mailing List ///////////////////////////");

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	//////////////////		
		
		System.out.println("///////////////////////////Initialize varialble///////////////////////////");
		
		
		int[] UUIDallDB_CommentsCount= new int[UUIDallDB.length];
		int[] UUIDallDB_ChangesCount= new int[UUIDallDB.length];
		int[] UUIDallDB_IssuesSubmittedCount= new int[UUIDallDB.length];
		int[] UUIDallDB_IssuesAssigned= new int[UUIDallDB.length];
		
		
		int[] UUIDallDB_CommitCount=new int[UUIDallDB.length];
		String[] UUIDallDB_CommitInterval=new String[UUIDallDB.length];
		int[] UUIDallDB_RepositoryCount=new int[UUIDallDB.length];
		
		
		int[] UUIDallDB_Messages_Sent_Count=new int[UUIDallDB.length];
		String[] UUIDallDB_MessageInterval=new String[UUIDallDB.length];
		int[] UUIDallDB_MailingListUsedCount=new int[UUIDallDB.length];
		System.out.println("///////////////////////////Initialize varialble///////////////////////////");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
	
		
		System.out.println("///////////////////////////Generate Results///////////////////////////");
		for(int x=0;x<UUIDallDB.length;x++){
			outerLoop:
			for(int y=0;y<TDB_UUID.length;y++){
				if(UUIDallDB[x].equals(TDB_UUID[y])){
					UUIDallDB_CommentsCount[x]=TDB_CommentsCount[y];
					UUIDallDB_ChangesCount[x]=TDB_ChangesCount[y];
					UUIDallDB_IssuesSubmittedCount[x]=TDB_IssuesSubmittedCount[y];
					UUIDallDB_IssuesAssigned[x]=TDB_IssuesAssigned[y];
					break outerLoop;
				}else{
					UUIDallDB_CommentsCount[x]=0;
					UUIDallDB_ChangesCount[x]=0;
					UUIDallDB_IssuesSubmittedCount[x]=0;
					UUIDallDB_IssuesAssigned[x]=0;
				}
			}
		}
		
		
		for(int x=0;x<UUIDallDB.length;x++){
			outerLoop:
			for(int y=0;y<SC_UUID.length;y++){
				if(UUIDallDB[x].equals(SC_UUID[y])){
					UUIDallDB_CommitCount[x]=SC_CommitCount[y];
					UUIDallDB_CommitInterval[x]=SC_CommitInterval[y];
					UUIDallDB_RepositoryCount[x]=SC_RepositoryCount[y];
					break outerLoop;
				}else{
					UUIDallDB_CommitCount[x]=0;
					UUIDallDB_CommitInterval[x]="N/A";
					UUIDallDB_RepositoryCount[x]=0;
				}
			}
		}
		
		for(int x=0;x<UUIDallDB.length;x++){
			outerLoop:
			for(int y=0;y<ML_UUID.length;y++){
				if(UUIDallDB[x].equals(ML_UUID[y])){
					UUIDallDB_Messages_Sent_Count[x]=ML_Messages_Sent_Count[y];
					UUIDallDB_MessageInterval[x]=ML_MessageInterval[y];
					UUIDallDB_MailingListUsedCount[x]=ML_MailingListUsedCount[y];
					break outerLoop;
				}else{
					UUIDallDB_Messages_Sent_Count[x]=0;
					UUIDallDB_MessageInterval[x]="N/A";
					UUIDallDB_MailingListUsedCount[x]=0;
				}
			}
		}
		System.out.println("///////////////////////////Generate Results///////////////////////////");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		System.out.println("///////////////////////////Print Results///////////////////////////");

		
		for(int x=0;x<UUIDallDB.length;x++){
			System.out.println("ROW : "+x+"\t\tUUID : "+UUIDallDB[x]+"\t\tComments : "+UUIDallDB_CommentsCount[x]+"\t\tChanges : "+UUIDallDB_ChangesCount[x]+"\t\tIssues Submitted : "+UUIDallDB_IssuesSubmittedCount[x]+"\t\tIssues Assigned : "+UUIDallDB_IssuesAssigned[x]+
					"\t\tCommit Count : "+UUIDallDB_CommitCount[x]+"\t\tCommit Interval(Hours) : "+UUIDallDB_CommitInterval[x]+"\t\tReposotory Count : "+UUIDallDB_RepositoryCount[x]+"\t\tMessages Sent Count : "+UUIDallDB_Messages_Sent_Count[x]+
					"\t\tMessage Interval(Hours) : "+UUIDallDB_MessageInterval[x]+"\t\tMailing List Used : "+UUIDallDB_MailingListUsedCount[x]);
		}
		System.out.println("///////////////////////////Complete///////////////////////////");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////				 	
		
		try {

			PrintWriter writer = new PrintWriter(
			new FileWriter("C:\\Users\\moses\\Desktop\\all_db_findings.csv", true));
		
			writer.append("ROW");
			writer.append(",");
			writer.append("UUID");
			writer.append(",");
			writer.append("Comments");
			writer.append(",");
			writer.append("Changes");
			writer.append(",");
			writer.append("Issues Submitted");
			writer.append(",");
			writer.append("Issues Assigned");
			writer.append(",");
			writer.append("Commit Count");
			writer.append(",");
			writer.append("Commit Interval(Hours)");
			writer.append(",");
			writer.append("Repository Count");
			writer.append(",");
			writer.append("Messages Sent Count");
			writer.append(",");
			writer.append("Message Interval(Hours)");
			writer.append(",");
			writer.append("Mailing List Used");
			writer.append("\n");
						
			
			for(int x=0;x<UUIDallDB.length;x++){
				writer.append(""+x);
				writer.append(",");
				writer.append(UUIDallDB[x]);
				writer.append(",");
				writer.append(""+UUIDallDB_CommentsCount[x]);
				writer.append(",");
				writer.append(""+UUIDallDB_ChangesCount[x]);
				writer.append(",");
				writer.append(""+UUIDallDB_IssuesSubmittedCount[x]);
				writer.append(",");
				writer.append(""+UUIDallDB_IssuesAssigned[x]);
				writer.append(",");
				writer.append(""+UUIDallDB_CommitCount[x]);
				writer.append(",");
				writer.append(""+UUIDallDB_CommitInterval[x]);
				writer.append(",");
				writer.append(""+UUIDallDB_RepositoryCount[x]);
				writer.append(",");
				writer.append(""+UUIDallDB_Messages_Sent_Count[x]);
				writer.append(",");
				writer.append(""+UUIDallDB_MessageInterval[x]);
				writer.append(",");
				writer.append(""+UUIDallDB_MailingListUsedCount[x]);
				writer.append("\n");
			}

		

			writer.close();
			System.out.println("done");
			
		} catch (IOException E) {
		
		}
		
		System.out.println("done");
	
	
	}



}
