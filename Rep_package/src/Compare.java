import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class Compare {

	public static void main(){
		

		
///////Read Database for Row Count and Collumn names//////////////////////////////////////////////////////////////////////////////////////////		
		
		String[] colName = GeneratetableCols(GenerateCollumnCount());
		System.out.println("Total No. Of Collumns = "+colName.length+"\n");
		for(int x=0;x<colName.length;x++){
			System.out.println("Collumn no. " +x+ " : "+colName[x]);
		}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		
		
///////Read User Input for actual and predicted collumn////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		Scanner scanner = new Scanner(System.in);
		
		
		int selectedActualCollumnNo;
		String selectedActualCollumnName;
		System.out.println("\n\nPlease Select collumn Number of Actual : ");
		selectedActualCollumnNo=Integer.parseInt(scanner.next());
		selectedActualCollumnName=colName[selectedActualCollumnNo];
		
		int selectedPredictedCollumnNo;
		String selectedPredictedCollumnName;
		System.out.println("\n\nPlease Select collumn Number of Predicted : ");
		selectedPredictedCollumnNo=Integer.parseInt(scanner.next());
		selectedPredictedCollumnName=colName[selectedPredictedCollumnNo];
		
		
		System.out.println("Collumn No. : "+selectedActualCollumnNo+", Collumn Name : "+selectedActualCollumnName +" Selected for Actual");
		System.out.println("Collumn No. : "+selectedPredictedCollumnNo+", Collumn Name : "+selectedPredictedCollumnName +" Selected for Predicted");
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		
///remove rows with actual =0 and predicted =0////////////////////////////////////////////////		
		double[] readActual= Compare.readActual(selectedActualCollumnName);
		double[] readPredicted= Compare.readPrediction(selectedPredictedCollumnName);	
		
		for(int x=0;x<readActual.length;x++){
			System.out.println(readActual[x]+"\t\t"+readPredicted[x]);
		}
		
		ArrayList<Double> readActualAL = new ArrayList<Double>();
		ArrayList<Double> readPredictedAL = new ArrayList<Double>();

		for(int x=0;x<readActual.length;x++){	
			if(readActual[x]!=0||readPredicted[x]!=0){
			readActualAL.add(readActual[x]);
			readPredictedAL.add(readPredicted[x]);
			}
		}
		
		double[] actual= new double[readActualAL.size()];
		double[] predicted= new double[readActualAL.size()];
		
		for(int x=0;x<readActualAL.size();x++){
			actual[x]=readActualAL.get(x);
			predicted[x]=readPredictedAL.get(x);
		}
	
		
		System.out.println("Starting Row Count :  "+readActual.length + "\tFinal Row Count : "+actual.length+"\tNo Of Rows Removed : "+(readActual.length-actual.length));
/////////////////////////////////////////////////////////////////////////////////////////////////////////
	

//////Generate Results//////////////////////////////////////////////////////////////////////////////////
		meanAbsoluteError(actual,predicted);
		
		meanAbsolutePercentageError(actual,predicted);
		
		rootMeanSquaredError(actual,predicted);
		
		meanSquaredError(actual,predicted);
////////////////////////////////////////////////////////////////////////////////
		
	}	
	
	
	public static String[] GeneratetableCols(int totalCol){
	
		
		
		String[] colName = new String[totalCol];
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			
			ResultSet rs = stmt.executeQuery("describe replicate_package.test2");
			
			
			for(int x=0;rs.next();x++){
				colName[x]=rs.getString("field");
			}
		
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
	return colName;
	}
	public static int GenerateCollumnCount(){
		
	
		int totalCol=0;
		
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			
			ResultSet rs = stmt.executeQuery("describe replicate_package.test2");
			
			while(rs.next()){		
				totalCol++;
			}
			
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
	return totalCol;
	}
	public static int GenerateTotalRows(){
	
		//System.out.println("Generate Total Rows");
		int totalRow=0;
		
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT count(*) as totalRow FROM replicate_package.test;");
			
			while(rs.next()){		
				totalRow=rs.getInt("totalRow");
			}
			
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
	return totalRow;
	}	
	public static double[] readActual(String actualColName){
		//System.out.println("Read Actual");
		int totalRow=Compare.GenerateTotalRows();	
		
		double[] actual=new double[totalRow];
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			//ResultSet rs = stmt.executeQuery("SELECT replicate_package.test.actual FROM replicate_package.test;");
			ResultSet rs = stmt.executeQuery("SELECT replicate_package.test2."+actualColName+" as actual FROM replicate_package.test2;");
			for(int x=0;rs.next();x++){
				actual[x]=rs.getDouble("actual");
			}		
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	return actual;
	}	
	public static double[] readPrediction(String predictedColName){
		//System.out.println("Read Prediction");
		int totalRow=Compare.GenerateTotalRows();
		
		double[] prediction=new double[totalRow];
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			//ResultSet rs = stmt.executeQuery("SELECT replicate_package.test.predicted FROM replicate_package.test;");
			ResultSet rs = stmt.executeQuery("SELECT replicate_package.test2."+predictedColName+" as predicted FROM replicate_package.test2;");
			
			for(int x=0;rs.next();x++){
				prediction[x]=rs.getDouble("predicted");
			}
			
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	return prediction;
	}
	
	public static void meanAbsoluteError(double[] actual,double[] predicted){
		System.out.println("\n\nMean Absolute Error\n");
		
		double[] absoluteResidual= new double[actual.length];
		double totalAbsoluteResidual=0;
		double MAE=0;//Mean Absolute Error
		
		for(int x=0;x<actual.length;x++){
			absoluteResidual[x]=Math.abs((predicted[x]-actual[x]));
			
			totalAbsoluteResidual=totalAbsoluteResidual+absoluteResidual[x];
			//System.out.println("Actual Length : "+actual[x]+"\t\t\tPredicted : "+predicted[x]+"\t\t\tResidual : "+absoluteResidual[x]+"\n");
		}
		MAE=totalAbsoluteResidual/actual.length;
		System.out.println("MAE : "+MAE);
	}
	
	public static void meanAbsolutePercentageError(double[] actual,double[] predicted){
		System.out.println("\n\nMean Absolute Percentage Error\n");
		
		
		
		double[] absoluteResidual = new double[actual.length];
		
		double MAPE =0 ;

		for(int x=0;x<actual.length;x++){
			if(actual[x]==0&&predicted[x]==0){
				System.out.println("");
			}
	//		System.out.println(x+ " Actual : "+actual[x]+"\tPredicted : "+predicted[x]);
			absoluteResidual[x]=Math.abs((((actual[x]-predicted[x])/actual[x])*100));
		
			
//			System.out.println(absoluteResidual[x]);
		};
		for(int x=0;x<actual.length;x++){
			MAPE=MAPE+absoluteResidual[x];
		}

		MAPE=MAPE/actual.length;
		System.out.println("MAPE : "+MAPE);
	
	}
	
	public static void rootMeanSquaredError(double[] actual,double[] predicted){
		System.out.println("\n\nRoot Mean Square Error \n");
		
		
		double[] residual = new double[actual.length];//Absolute Percent Error
		double totalSquaredResidual=0;
		double RMSE=0;
		
		for(int x=0;x<actual.length;x++){
			residual[x]=actual[x]-predicted[x];
		}
		for(int x=0;x<actual.length;x++){
			totalSquaredResidual=totalSquaredResidual+(residual[x]*residual[x]);
		}
		RMSE= Math.sqrt(totalSquaredResidual/(actual.length-1));
		System.out.println("RMSE : "+RMSE);
		
		
	}
	public static void meanSquaredError(double[] actual,double[] predicted){
		System.out.println("\n\nMean Square Error \n");
		
	
		
		double[] SE=new double[actual.length];//Squared error
		
		double TSE=0;//Total Squared Error
		
		double[] residual=new double[actual.length];
		
		double MSE=0;//Mean Squared Error
		
		for(int x=0;x<actual.length;x++){
			residual[x]=actual[x]-predicted[x];
			SE[x]=residual[x]*residual[x];
		}
		
		for(int x=0;x<actual.length;x++){
			TSE=TSE+SE[x];
		}
		
		MSE=TSE/actual.length;
		System.out.println("Mean Squared Error : "+ MSE);
		
	}
	
}
