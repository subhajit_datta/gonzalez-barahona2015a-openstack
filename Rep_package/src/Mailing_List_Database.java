
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class Mailing_List_Database {
public static void Main(){
		
		System.out.println("Mailing List Database");
		
		String[] UUID =GenerateMailingListUUID();			
		
		System.out.println("UUID Length : "+UUID.length); 
		 
		int[] MessagesSentCount =GenerateMessagesSentCount(UUID);
		System.out.println("Messages Sent Length : "+MessagesSentCount.length);
		
		int[] MailingListUsedCount = GenerateMailingListUsedCount(UUID);
		System.out.println("Mailing List Used Length : "+MailingListUsedCount.length);
		
		
		String[] MessagesInterval = generateMessagesInterval(UUID);
		System.out.println("Message Interval Length : "+MessagesInterval.length);
		
		for(int x=0;x<UUID.length;x++){
			System.out.println("ROW : "+x+"\t\tUUID : "+UUID[x]+"\t\tMessage Count : "+MessagesSentCount[x]+"\t\tMailing List Used Count : "+MailingListUsedCount[x]+"\t\tMessage Interval : "+MessagesInterval[x]);
		}
		
		
	}
	
	public static String[] GenerateMailingListUUID(){
		int rowCount=0;
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT mailing_list.people_uidentities.uuid,count(*) as counts FROM mailing_list.people_uidentities group by mailing_list.people_uidentities.uuid;");
			
			while(rs.next()){		
				rowCount++;
			}
			
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	//////////////////	//////////////////////////////////////////////////////	//////////////////		
		String UUID[] = new String[rowCount];
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT mailing_list.people_uidentities.uuid,count(*) as counts FROM mailing_list.people_uidentities group by mailing_list.people_uidentities.uuid;");
			
			for(int x=0;rs.next();x++){
				UUID[x]=rs.getString("uuid");
				//System.out.println(x+"\t"+rs.getString("uuid"));
			}			
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		return UUID;
	}

	public static int[] GenerateMessagesSentCount(String[] MailingListUUID){
		int rowCount=0;
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT mailing_list.people_uidentities.uuid,count(*) as counts FROM mailing_list.people_uidentities INNER JOIN mailing_list.messages_people ON mailing_list.people_uidentities.people_id = mailing_list.messages_people.email_address group by mailing_list.people_uidentities.uuid;");
			
			while(rs.next()){		
				rowCount++;
			}
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	//////////////////////////////////////////////////////	//////////////////		
		int[] MessageSentCount=new int[rowCount];
		String[] UUID=new String[rowCount];
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT mailing_list.people_uidentities.uuid,count(*) as counts FROM mailing_list.people_uidentities INNER JOIN mailing_list.messages_people ON mailing_list.people_uidentities.people_id = mailing_list.messages_people.email_address group by mailing_list.people_uidentities.uuid;");
			
			for(int x=0;rs.next();x++){
				MessageSentCount[x]=rs.getInt("counts");
				UUID[x]=rs.getString("uuid");
			}
			
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	//////////////////		
		
		int[] totalMessagesCount= new int[MailingListUUID.length];
		
		for(int x=0;x<totalMessagesCount.length;x++){
			totalMessagesCount[x]=0;
		}
		
		for(int x=0;x<MailingListUUID.length;x++){
			for(int y=0;y<UUID.length;y++){
				if(MailingListUUID[x].equals(UUID[y])){
					totalMessagesCount[x]=totalMessagesCount[x]+MessageSentCount[y];
				}
			}
		}
		/*
		for(int x=0;x<MailingListUUID.length;x++){
			System.out.println("ROW : "+x+"\t\tUUID : "+UUID[x]+"\t\tMessages Sent : "+totalMessagesCount[x]);
		}
		*/
		return totalMessagesCount;
	
	}
	
	public static int[] GenerateMailingListUsedCount(String[] MailingListUUID){
		int rowCount=0;
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT mailing_list.people_uidentities.uuid,count(distinct(mailing_list.messages_people.mailing_list_url)) as counts FROM mailing_list.people_uidentities INNER JOIN mailing_list.messages_people ON mailing_list.people_uidentities.people_id = mailing_list.messages_people.email_address group by mailing_list.people_uidentities.uuid;");
			
			while(rs.next()){		
				rowCount++;
			}
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	//////////////////////////////////////////////////////	//////////////////		
		int[] MailingListUsed=new int[rowCount];
		String[] UUID=new String[rowCount];
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT mailing_list.people_uidentities.uuid,count(distinct(mailing_list.messages_people.mailing_list_url)) as counts FROM mailing_list.people_uidentities INNER JOIN mailing_list.messages_people ON mailing_list.people_uidentities.people_id = mailing_list.messages_people.email_address group by mailing_list.people_uidentities.uuid;");
			
			for(int x=0;rs.next();x++){
				MailingListUsed[x]=rs.getInt("counts");
				UUID[x]=rs.getString("uuid");
			}
			
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	//////////////////		
		
		int[] totalMailingListUsed= new int[MailingListUUID.length];
		
		for(int x=0;x<MailingListUUID.length;x++){
			totalMailingListUsed[x]=0;
		}
		
		for(int x=0;x<MailingListUUID.length;x++){
			for(int y=0;y<UUID.length;y++){
				if(MailingListUUID[x].equals(UUID[y])){
					totalMailingListUsed[x]=totalMailingListUsed[x]+MailingListUsed[y];
				}
			}
		}
		
		/*
		for(int x=0;x<MailingListUUID.length;x++){
			System.out.println("ROW : "+x+"\t\tUUID : "+UUID[x]+"\t\tMailing List Used : "+totalMailingListUsed[x]);
		}
		*/
		return totalMailingListUsed;
		
	}

	public static String[] generateMessagesInterval(String[] MailingListUUID){
//SELECT mailing_list.people_uidentities.uuid,max(mailing_list.messages.first_date) FROM mailing_list.people_uidentities INNER JOIN mailing_list.messages_people ON mailing_list.people_uidentities.people_id = mailing_list.messages_people.email_address INNER JOIN mailing_list.messages ON mailing_list.messages_people.message_id = mailing_list.messages.message_ID group by mailing_list.people_uidentities.uuid;
		
		
		int rowCount=0;
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT mailing_list.people_uidentities.uuid,max(mailing_list.messages.first_date) as date FROM mailing_list.people_uidentities INNER JOIN mailing_list.messages_people ON mailing_list.people_uidentities.people_id = mailing_list.messages_people.email_address INNER JOIN mailing_list.messages ON mailing_list.messages_people.message_id = mailing_list.messages.message_ID group by mailing_list.people_uidentities.uuid;");
			
			while(rs.next()){		
				rowCount++;
			}
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	//////////////////////////////////////////////////////	//////////////////		
		
		String[] UUID= new String[rowCount];
		String[] FirstMessageDate= new String[rowCount];
		String[] LastMessageDate= new String[rowCount];
		long[] hourInterval = new long[rowCount];
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT mailing_list.people_uidentities.uuid,min(mailing_list.messages.first_date) as date FROM mailing_list.people_uidentities INNER JOIN mailing_list.messages_people ON mailing_list.people_uidentities.people_id = mailing_list.messages_people.email_address INNER JOIN mailing_list.messages ON mailing_list.messages_people.message_id = mailing_list.messages.message_ID group by mailing_list.people_uidentities.uuid;");
		
			for(int x=0;rs.next();x++){
				UUID[x]=rs.getString("uuid");
				FirstMessageDate[x]=rs.getString("date");
			}
			
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT mailing_list.people_uidentities.uuid,max(mailing_list.messages.first_date) as date FROM mailing_list.people_uidentities INNER JOIN mailing_list.messages_people ON mailing_list.people_uidentities.people_id = mailing_list.messages_people.email_address INNER JOIN mailing_list.messages ON mailing_list.messages_people.message_id = mailing_list.messages.message_ID group by mailing_list.people_uidentities.uuid;");
		
			for(int x=0;rs.next();x++){
				LastMessageDate[x]=rs.getString("date");
			}
			
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	//////////////////		

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.0"); 
		for(int x=0;x<UUID.length;x++){
			 LocalDateTime firstMessage = LocalDateTime.parse(FirstMessageDate[x], formatter);
			 LocalDateTime lastMessage = LocalDateTime.parse(LastMessageDate[x], formatter );
			 long minutes = Duration.between(firstMessage, lastMessage).toHours();
			 hourInterval[x]=minutes;
		}
		
		String[] FirstLastMessageHoursInterval = new String[MailingListUUID.length];
		
		for(int x=0;x<MailingListUUID.length;x++){			
			outerLoop:
			for(int y=0;y<UUID.length;y++){
				if(MailingListUUID[x].equals(UUID[y])){
					FirstLastMessageHoursInterval[x]=Long.toString(hourInterval[y]);
					break outerLoop;
				}else{
					//FirstLastMessageHoursInterval[x]="N/A";
					FirstLastMessageHoursInterval[x]="0";
				}
			}
		}
		/*
		for(int x=0;x<UUID.length;x++){
			System.out.println("ROW : "+x+"\t\tUUID : "+UUID[x]+"\t\tFirst Message : "+FirstMessageDate[x]+"\t\tLast Message : "+LastMessageDate[x]+"\t\tHours Interval : "+FirstLastMessageHoursInterval[x]);
		}
		*/
		return FirstLastMessageHoursInterval;
		
	}
}
