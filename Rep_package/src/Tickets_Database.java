
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class Tickets_Database {
	
	
	public static void Main(){
		
System.out.println("tickets Database");
		//String ticketsUUID[] = GenerateTicketsUUID();
		//print(ticketsUUID,GenerateUuidTotalChanges(ticketsUUID),GenerateUuidTotalComments(ticketsUUID),GenerateUuidTotalSubmittedIssues(ticketsUUID),GenerateUuidTotalAssignedIssues(ticketsUUID));
GenerateUuidTotalChanges(GenerateTicketsUUID());
	}

	
	
	public static String[] GenerateTicketsUUID(){
		
		int rowCount=0;
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT *, count(tickets.tickets_pid_uuid_people.uuid) as counts FROM tickets.tickets_pid_uuid_people group by tickets.tickets_pid_uuid_people.uuid;");
			
			while(rs.next()){		
				rowCount++;
			}
			
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
			
		String UUID[] = new String[rowCount];
	
		try {
			
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT tickets.tickets_pid_uuid_people.uuid FROM tickets.tickets_pid_uuid_people group by tickets.tickets_pid_uuid_people.uuid order by tickets.tickets_pid_uuid_people.uuid;");
					
			for(int x=0;rs.next();x++){
				UUID[x]=rs.getString("uuid");				
			}
			
			
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return UUID;
	}
	
	public static int[] GenerateUuidTotalChanges(String[] TicketsUUID){

		int rowCount=0;
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			//ResultSet rs = stmt.executeQuery("select * from tickets.comments order by tickets.comments.submitted_on");
			ResultSet rs = stmt.executeQuery("select tickets.tickets_pid_uuid_people.uuid FROM tickets.changes INNER JOIN tickets.tickets_pid_uuid_people ON tickets.changes.changed_by = tickets.tickets_pid_uuid_people.people_id GROUP BY tickets.changes.changed_by order by tickets.tickets_pid_uuid_people.uuid;");
			
			while(rs.next()){
				rowCount++;
			}
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		String PID[] = new String[rowCount];
		String UUID[] = new String[rowCount];
		String Name[] = new String[rowCount];
		int ChangesCount[] = new int[rowCount];
		
		
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			//ResultSet rs = stmt.executeQuery("select * from tickets.comments order by tickets.comments.submitted_on");
			ResultSet rs = stmt.executeQuery("SELECT tickets.changes.changed_by, tickets.tickets_pid_uuid_people.uuid,tickets.tickets_pid_uuid_people.name, COUNT(tickets.changes.id) AS counts FROM tickets.changes INNER JOIN tickets.tickets_pid_uuid_people ON tickets.changes.changed_by = tickets.tickets_pid_uuid_people.people_id GROUP BY tickets.changes.changed_by order by tickets.tickets_pid_uuid_people.uuid;");
			
			
			for(int x=0;rs.next();x++){
				PID[x]=rs.getString("changed_by");
				UUID[x]=rs.getString("uuid");
				Name[x]=rs.getString("name");
				ChangesCount[x]=Integer.parseInt(rs.getString("counts"))  ;
			}
			
			/*
			for(int x=0;x<rowCount;x++){
				//System.out.println("x : "+x+"\tPID : "+PID[x]+"\tUUID : "+UUID[x]+"\tName : "+Name[x]+"\t\t\tCounts : "+ChangesCount[x]);
			
				if(UUID[x].equals("06ba8d6edf46313b23458d3e80cd2321ec1c1d44")||UUID[x].equals("0b7a611224bbbbc39c819851c303e0ba19bb3a95")||UUID[x].equals("e01e73c05125af075f6bceedc19a1d96852d6733")){
					System.out.println("x : "+x+"\tPID : "+PID[x]+"\tUUID : "+UUID[x]+"\tName : "+Name[x]+"\t\t\tCounts : "+ChangesCount[x]);
				}
			}*/
			
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		

		int UUID_TotalChangeCount[]= new int[TicketsUUID.length];
		
		
		for(int x=0;x<TicketsUUID.length;x++){
			UUID_TotalChangeCount[x]=0;
		}
		
		for(int x=0;x<TicketsUUID.length;x++){
			for(int y=0;y<UUID.length;y++){
				if(TicketsUUID[x].equals(UUID[y])){
					UUID_TotalChangeCount[x]=UUID_TotalChangeCount[x]+ChangesCount[y];
				}
			}
		}
		/*
		for(int x=0;x<TicketsUUID.length;x++){
			
			System.out.println("x : "+x+"\tUUID : "+TicketsUUID[x]+"\t\tChanges Count : "+UUID_TotalChangeCount[x]);
			
			
			if(TicketsUUID[x].equals("06ba8d6edf46313b23458d3e80cd2321ec1c1d44")||TicketsUUID[x].equals("0b7a611224bbbbc39c819851c303e0ba19bb3a95")||TicketsUUID[x].equals("e01e73c05125af075f6bceedc19a1d96852d6733")){
				System.out.println("x : "+x+"\tUUID : "+TicketsUUID[x]+"\t\t TOTAL Changes Count : "+UUID_TotalChangeCount[x]);
			}
			
		}*/
		return UUID_TotalChangeCount;
	}

	public static int[] GenerateUuidTotalComments(String[] TicketsUUID){

		int rowCount=0;
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("select tickets.tickets_pid_uuid_people.uuid FROM tickets.comments INNER JOIN tickets.tickets_pid_uuid_people ON tickets.comments.submitted_by = tickets.tickets_pid_uuid_people.people_id group BY tickets.comments.submitted_by order by tickets.tickets_pid_uuid_people.uuid;");
			
			while(rs.next()){
				rowCount++;
			}
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
		String PID[] = new String[rowCount];
		String UUID[] = new String[rowCount];
		String Name[] = new String[rowCount];
		int CommentsCount[] = new int[rowCount];
		
		
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT tickets.comments.submitted_by, tickets.tickets_pid_uuid_people.uuid,tickets.tickets_pid_uuid_people.name, COUNT(tickets.comments.id) AS counts FROM tickets.comments INNER JOIN tickets.tickets_pid_uuid_people ON tickets.comments.submitted_by = tickets.tickets_pid_uuid_people.people_id GROUP BY tickets.comments.submitted_by order by tickets.tickets_pid_uuid_people.uuid;");
			
			
			for(int x=0;rs.next();x++){
				PID[x]=rs.getString("submitted_by");
				UUID[x]=rs.getString("uuid");
				Name[x]=rs.getString("name");
				CommentsCount[x]=Integer.parseInt(rs.getString("counts"))  ;
			}
			
			/*
			for(int x=0;x<rowCount;x++){
				//System.out.println("x : "+x+"\tPID : "+PID[x]+"\tUUID : "+UUID[x]+"\tName : "+Name[x]+"\t\t\tCounts : "+ChangesCount[x]);
				
				if(UUID[x].equals("06ba8d6edf46313b23458d3e80cd2321ec1c1d44")||UUID[x].equals("0b7a611224bbbbc39c819851c303e0ba19bb3a95")||UUID[x].equals("e01e73c05125af075f6bceedc19a1d96852d6733")||UUID[x].equals("efcc6b7b054010680eb1bc039e532c233e428c4f")){
					System.out.println("x : "+x+"\tPID : "+PID[x]+"\tUUID : "+UUID[x]+"\tName : "+Name[x]+"\t\t\tCounts : "+CommentsCount[x]);
				}
			}
			*/
			
			
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
		int UUID_TotalCommentCount[]= new int[TicketsUUID.length];
		
		
		for(int x=0;x<TicketsUUID.length;x++){
			UUID_TotalCommentCount[x]=0;
		}
		
		for(int x=0;x<TicketsUUID.length;x++){
			for(int y=0;y<UUID.length;y++){
				if(TicketsUUID[x].equals(UUID[y])){
					UUID_TotalCommentCount[x]=UUID_TotalCommentCount[x]+CommentsCount[y];
				}
			}
		}
		/*
		for(int x=0;x<TicketsUUID.length;x++){
			
			System.out.println("x : "+x+"\tUUID : "+TicketsUUID[x]+"\t\tComment Count : "+UUID_TotalCommentCount[x]);
			
			
			if(TicketsUUID[x].equals("06ba8d6edf46313b23458d3e80cd2321ec1c1d44")||TicketsUUID[x].equals("0b7a611224bbbbc39c819851c303e0ba19bb3a95")||TicketsUUID[x].equals("e01e73c05125af075f6bceedc19a1d96852d6733")||TicketsUUID[x].equals("efcc6b7b054010680eb1bc039e532c233e428c4f") ){
				System.out.println("x : "+x+"\tUUID : "+TicketsUUID[x]+"\t\t TOTAL Comment Count : "+UUID_TotalCommentCount[x]);
			}
			
		}*/
		
		return UUID_TotalCommentCount;
	}
	
	public static int[] GenerateUuidTotalSubmittedIssues(String[] TicketsUUID){
	
			int rowCount=0;
			
			try {
				Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");
	
				Statement stmt = (Statement) con.createStatement();
				
				ResultSet rs = stmt.executeQuery("select * FROM tickets.issues INNER JOIN tickets.tickets_pid_uuid_people ON tickets.issues.submitted_by = tickets.tickets_pid_uuid_people.people_id group BY tickets.issues.submitted_by order by tickets.tickets_pid_uuid_people.uuid;");
				
				while(rs.next()){
					rowCount++;
				}
			}
			
			catch (SQLException e) {
				e.printStackTrace();
			}
	
			
			
			
			String PID[] = new String[rowCount];
			String UUID[] = new String[rowCount];
			String Name[] = new String[rowCount];
			int IssuesSubmittedCount[] = new int[rowCount];
			
			try {
				Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");
	
				Statement stmt = (Statement) con.createStatement();
				
			
				ResultSet rs = stmt.executeQuery("SELECT tickets.issues.submitted_by,tickets.tickets_pid_uuid_people.uuid,tickets.tickets_pid_uuid_people.name,COUNT(tickets.issues.submitted_by) AS counts FROM tickets.issues INNER JOIN tickets.tickets_pid_uuid_people ON tickets.issues.submitted_by = tickets.tickets_pid_uuid_people.people_id GROUP BY tickets.issues.submitted_by ORDER BY tickets.tickets_pid_uuid_people.uuid;");
				
				
				for(int x=0;rs.next();x++){
					PID[x]=rs.getString("submitted_by");
					UUID[x]=rs.getString("uuid");
					Name[x]=rs.getString("name");
					IssuesSubmittedCount[x]=Integer.parseInt(rs.getString("counts"))  ;
				}
				
				/*
				for(int x=0;x<rowCount;x++){
					System.out.println("x : "+x+"\tPID : "+PID[x]+"\tUUID : "+UUID[x]+"\tName : "+Name[x]+"\t\t\tCounts : "+ChangesCount[x]);
					
					
					if(UUID[x].equals("06ba8d6edf46313b23458d3e80cd2321ec1c1d44")||UUID[x].equals("0b7a611224bbbbc39c819851c303e0ba19bb3a95")||UUID[x].equals("e01e73c05125af075f6bceedc19a1d96852d6733")||UUID[x].equals("efcc6b7b054010680eb1bc039e532c233e428c4f")){
						System.out.println("x : "+x+"\tPID : "+PID[x]+"\tUUID : "+UUID[x]+"\tName : "+Name[x]+"\t\t\tCounts : "+IssuesSubmittedCount[x]);
					}
				}
				*/
			}
			
			catch (SQLException e) {
				e.printStackTrace();
			}
			
			
			
			int UUID_TotalIssuesSubmittedCount[]= new int[TicketsUUID.length];
			
			
			for(int x=0;x<TicketsUUID.length;x++){
				UUID_TotalIssuesSubmittedCount[x]=0;
			}
			
			for(int x=0;x<TicketsUUID.length;x++){
				for(int y=0;y<UUID.length;y++){
					if(TicketsUUID[x].equals(UUID[y])){
						UUID_TotalIssuesSubmittedCount[x]=UUID_TotalIssuesSubmittedCount[x]+IssuesSubmittedCount[y];
					}
				}
			}
			
			/*
			for(int x=0;x<TicketsUUID.length;x++){
				
				System.out.println("x : "+x+"\tUUID : "+TicketsUUID[x]+"\t\tTOTAL Issues Submitted Count : "+UUID_TotalIssuesSubmittedCount[x]);
				
				
				if(TicketsUUID[x].equals("06ba8d6edf46313b23458d3e80cd2321ec1c1d44")||TicketsUUID[x].equals("0b7a611224bbbbc39c819851c303e0ba19bb3a95")||TicketsUUID[x].equals("e01e73c05125af075f6bceedc19a1d96852d6733")||TicketsUUID[x].equals("efcc6b7b054010680eb1bc039e532c233e428c4f") ){
					System.out.println("x : "+x+"\tUUID : "+TicketsUUID[x]+"\t\t TOTAL Issues Submitted Count : "+UUID_TotalIssuesSubmittedCount[x]);
				}
				
			}
			*/
			return UUID_TotalIssuesSubmittedCount;
		}

	public static int[] GenerateUuidTotalAssignedIssues(String[] TicketsUUID){
		
		int rowCount=0;
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
			ResultSet rs = stmt.executeQuery("select * FROM tickets.issues INNER JOIN tickets.tickets_pid_uuid_people ON tickets.issues.assigned_to = tickets.tickets_pid_uuid_people.people_id group BY tickets.issues.assigned_to order by tickets.tickets_pid_uuid_people.uuid;");
			
			while(rs.next()){
				rowCount++;
			}
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
	
		
		
		
		String PID[] = new String[rowCount];
		String UUID[] = new String[rowCount];
		String Name[] = new String[rowCount];
		int IssuesAssignedCount[] = new int[rowCount];
		
		try {
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tickets", "root", "mosesban");

			Statement stmt = (Statement) con.createStatement();
			
		
			ResultSet rs = stmt.executeQuery("SELECT tickets.issues.assigned_to,tickets.tickets_pid_uuid_people.uuid,tickets.tickets_pid_uuid_people.name,COUNT(tickets.issues.assigned_to) AS counts FROM tickets.issues INNER JOIN tickets.tickets_pid_uuid_people ON tickets.issues.assigned_to = tickets.tickets_pid_uuid_people.people_id GROUP BY tickets.issues.assigned_to ORDER BY tickets.tickets_pid_uuid_people.uuid;");
			
			
			for(int x=0;rs.next();x++){
				PID[x]=rs.getString("assigned_to");
				UUID[x]=rs.getString("uuid");
				Name[x]=rs.getString("name");
				IssuesAssignedCount[x]=Integer.parseInt(rs.getString("counts"))  ;
			}
			
			/*
			for(int x=0;x<rowCount;x++){
				//System.out.println("x : "+x+"\tPID : "+PID[x]+"\tUUID : "+UUID[x]+"\tName : "+Name[x]+"\t\t\tCounts : "+ChangesCount[x]);
				
				if(UUID[x].equals("06ba8d6edf46313b23458d3e80cd2321ec1c1d44")||UUID[x].equals("0b7a611224bbbbc39c819851c303e0ba19bb3a95")||UUID[x].equals("e01e73c05125af075f6bceedc19a1d96852d6733")||UUID[x].equals("efcc6b7b054010680eb1bc039e532c233e428c4f")){
					System.out.println("x : "+x+"\t\tPID : "+PID[x]+"\tUUID : "+UUID[x]+"\tName : "+Name[x]+"\t\t\tCounts : "+IssuesAssignedCount[x]);
				}
			}
			*/
			
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		int UUID_TotalIssuesAssignedCount[]= new int[TicketsUUID.length];
		
		
		for(int x=0;x<TicketsUUID.length;x++){
			UUID_TotalIssuesAssignedCount[x]=0;
		}
		
		for(int x=0;x<TicketsUUID.length;x++){
			for(int y=0;y<UUID.length;y++){
				if(TicketsUUID[x].equals(UUID[y])){
					UUID_TotalIssuesAssignedCount[x]=UUID_TotalIssuesAssignedCount[x]+IssuesAssignedCount[y];
				}
			}
		}
		/*
		for(int x=0;x<TicketsUUID.length;x++){
			
			//System.out.println("x : "+x+"\tUUID : "+TicketsUUID[x]+"\t\tTOTAL Issues Assigned Count : "+UUID_TotalIssuesSubmittedCount[x]);
			
			
			if(TicketsUUID[x].equals("06ba8d6edf46313b23458d3e80cd2321ec1c1d44")||TicketsUUID[x].equals("0b7a611224bbbbc39c819851c303e0ba19bb3a95")||TicketsUUID[x].equals("e01e73c05125af075f6bceedc19a1d96852d6733")||TicketsUUID[x].equals("efcc6b7b054010680eb1bc039e532c233e428c4f") ){
				System.out.println("x : "+x+"\tUUID : "+TicketsUUID[x]+"\t\t TOTAL Issues Assigned Count : "+UUID_TotalIssuesAssignedCount[x]);
			}
			
		}
		
	*/
		return UUID_TotalIssuesAssignedCount;
	}

	public static void print(String[] UUID, int[] totalChanges,int[] totalComments, int[]totalIssuesSubmitted,int[] totalIssuesAssigned){
		
		
		for(int x=0;x<UUID.length;x++){
			System.out.println("\t UUID : "+UUID[x]+"\t\t\t Total Changes : "+totalChanges[x]+"\t\t\t Total Comments : "+totalComments[x]+"\t\t\t Total Issues Submitted : "+totalIssuesSubmitted[x]+"\t\t\t Total Issues Assigned : "+totalIssuesAssigned[x]);
		}
		
		
		
		System.out.println("END PRINT");
	}


}
